#elasticsearch
variable "es_domain_name" {
  description = "nama domain es"
  default     = "mydomain"
}

variable "es_version" {
  description = "versi es yang digunakan"
  default     = "7.0"
}


variable "es_instance_type" {
  description = "type instance cluster yg free ada di default"
  type        = string
  default     = "t2.small.elasticsearch"
}

variable "es_instance_count" {
  description = "jumlah nodes didalam 1 cluster"
  default     = 1
}

variable "es_cluster_dedicated_master_enabled" {
  description = "set ke true jika mau pake master node"
  type        = bool
  default       = false
}

variable "es_cluster_dedicated_master_type" {
  description = "tipe instance untuk master node"
  type        = string
  default     = "r5.large.elasticsearch"
}