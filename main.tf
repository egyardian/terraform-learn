provider "aws" {
  region = "ap-southeast-1"
  #profile di ambil dari access key + secret key yg di register pake aws cli
}

module "coba-elasticsearch" {
  source  = "lgallard/elasticsearch/aws"
  version = "0.11.0"

  domain_name           = var.es_domain_name
  elasticsearch_version = var.es_version

  cluster_config = {
    dedicated_master_enabled = var.es_cluster_dedicated_master_enabled
    dedicated_master_type    = var.es_cluster_dedicated_master_type
    instance_type            = var.es_instance_type
    instance_count           = var.es_instance_count
  }


  #es_version variable dari elasticsearch_version dipake di module
  #dari basic terraform nya elasticsearch_version
  #es_version = "7.1"

  tags = {
    Name = "Test-elasticsearch"
  }

}
